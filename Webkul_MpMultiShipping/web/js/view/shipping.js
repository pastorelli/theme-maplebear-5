/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpMultiShipping
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*global define*/
define(
    [
        'jquery',
        'underscore',
        'Magento_Ui/js/form/form',
        'ko',
        'Magento_Customer/js/model/customer',
        'Magento_Customer/js/model/address-list',
        'Magento_Checkout/js/model/address-converter',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/action/create-shipping-address',
        'Magento_Checkout/js/action/select-shipping-address',
        'Magento_Checkout/js/model/shipping-rates-validator',
        'Magento_Checkout/js/model/shipping-address/form-popup-state',
        'Magento_Checkout/js/model/shipping-service',
        'Magento_Checkout/js/action/select-shipping-method',
        'Magento_Checkout/js/model/shipping-rate-registry',
        'Magento_Checkout/js/action/set-shipping-information',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Ui/js/modal/modal',
        'Magento_Checkout/js/model/checkout-data-resolver',
        'Magento_Checkout/js/checkout-data',
        'Magento_Catalog/js/price-utils',
        'uiRegistry',
        'mage/translate',
        'Magento_Checkout/js/model/shipping-rate-service',
        'Magento_Ui/js/modal/alert'
    ],
    function (
        $,
        _,
        Component,
        ko,
        customer,
        addressList,
        addressConverter,
        quote,
        createShippingAddress,
        selectShippingAddress,
        shippingRatesValidator,
        formPopUpState,
        shippingService,
        selectShippingMethodAction,
        rateRegistry,
        setShippingInformationAction,
        stepNavigator,
        modal,
        checkoutDataResolver,
        checkoutData,
        priceUtils,
        registry,
        $t,
        mageAlert
    ) {
        'use strict';

        var popUp = null;

        return Component.extend({
            defaults: {
                template: 'Webkul_MpMultiShipping/shipping'
            },
            visible: ko.observable(!quote.isVirtual()),
            errorValidationMessage: ko.observable(false),
            isCustomerLoggedIn: customer.isLoggedIn,
            isFormPopUpVisible: formPopUpState.isVisible,
            isFormInline: addressList().length === 0,
            isNewAddressAdded: ko.observable(false),
            saveInAddressBook: 1,
            quoteIsVirtual: quote.isVirtual(),
            selectedSellerMethod: ko.observableArray([]),
            totalSellerAmount: ko.observable(0),
            totalBaseSellerAmount: ko.observable(0),
            totalSeller: ko.observable(0),
            totalSelectedSeller: ko.observable(0),

            /**
             * @return {exports}
             */
            initialize: function () {
                var self = this,

                    hasNewAddress,
                    fieldsetName = 'checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset';
                window.scope = this;
                this._super();

                if (!quote.isVirtual()) {
                    stepNavigator.registerStep(
                        'shipping',
                        '',
                        $t('Shipping'),
                        this.visible,
                        _.bind(this.navigate, this),
                        this.sortOrder
                    );
                }
                checkoutDataResolver.resolveShippingAddress();

                hasNewAddress = addressList.some(function (address) {
                    return address.getType() == 'new-customer-address'; //eslint-disable-line eqeqeq
                });

                this.isNewAddressAdded(hasNewAddress);

                this.isFormPopUpVisible.subscribe(function (value) {
                    if (value) {
                        self.getPopUp().openModal();
                    }
                });

                quote.shippingMethod.subscribe(function () {
                    self.errorValidationMessage(false);
                });

                registry.async('checkoutProvider')(function (checkoutProvider) {
                    var shippingAddressData = checkoutData.getShippingAddressFromData();

                    if (shippingAddressData) {
                        checkoutProvider.set(
                            'shippingAddress',
                            $.extend(true, {}, checkoutProvider.get('shippingAddress'), shippingAddressData)
                        );
                    }
                    checkoutProvider.on('shippingAddress', function (shippingAddrsData) {
                        if (shippingAddrsData.street && !_.isEmpty(shippingAddrsData.street[0])) {
                            checkoutData.setShippingAddressFromData(shippingAddrsData);
                        }
                    });
                    shippingRatesValidator.initFields(fieldsetName);
                });

                return this;
            },

            /**
             * Navigator change hash handler.
             *
             * @param {Object} step - navigation step
             */
            navigate: function (step) {
                step && step.isVisible(true);
            },

            /**
             * @return {*}
             */
            getPopUp: function () {
                var self = this,
                    buttons;

                if (!popUp) {
                    buttons = this.popUpForm.options.buttons;
                    this.popUpForm.options.buttons = [
                        {
                            text: buttons.save.text ? buttons.save.text : $t('Save Address'),
                            class: buttons.save.class ? buttons.save.class : 'action primary action-save-address',
                            click: self.saveNewAddress.bind(self)
                        },
                        {
                            text: buttons.cancel.text ? buttons.cancel.text : $t('Cancel'),
                            class: buttons.cancel.class ? buttons.cancel.class : 'action secondary action-hide-popup',

                            /** @inheritdoc */
                            click: self.onClosePopUp.bind(self)
                        }
                    ];

                    /** @inheritdoc */
                    this.popUpForm.options.closed = function () {
                        self.isFormPopUpVisible(false);
                    };

                    this.popUpForm.options.modalCloseBtnHandler = this.onClosePopUp.bind(this);
                    this.popUpForm.options.keyEventHandlers = {
                        escapeKey: this.onClosePopUp.bind(this)
                    };

                    /** @inheritdoc */
                    this.popUpForm.options.opened = function () {
                        // Store temporary address for revert action in case when user click cancel action
                        self.temporaryAddress = $.extend(true, {}, checkoutData.getShippingAddressFromData());
                    };
                    popUp = modal(this.popUpForm.options, $(this.popUpForm.element));
                }

                return popUp;
            },

            /**
             * Revert address and close modal.
             */
            onClosePopUp: function () {
                checkoutData.setShippingAddressFromData($.extend(true, {}, this.temporaryAddress));
                this.getPopUp().closeModal();
            },

            /**
             * Show address form popup
             */
            showFormPopUp: function () {
                this.isFormPopUpVisible(true);
            },

            /**
             * Save new shipping address
             */
            saveNewAddress: function () {
                var addressData,
                    newShippingAddress;

                var self = this;

                this.source.set('params.invalid', false);
                this.triggerShippingDataValidateEvent();

                if (!this.source.get('params.invalid')) {
                    addressData = this.source.get('shippingAddress');

                    var xhttp = new XMLHttpRequest();

                    xhttp.open("GET", "https://api.svcs.biz/v1/location/" +
                        addressData.postcode.replace(/[^0-9]/gi, ''), true);

                    //Callback function to check readyState
                    xhttp.onreadystatechange = function() {
                        if (this.readyState === 4) {
                            if (this.status !== 200) {
                                var teste = $("[name='shippingAddress.postcode']").children().focus();

                                //remove the error div if exists to not duplicate
                                if($("[name='error-cep']"))
                                    $("[name='error-cep']").remove();

                                $("[name='shippingAddress.postcode']").append('<div class="mage-error" ' +
                                    'generated="true" name="error-cep" id="description-error">' +
                                    'Cep inválido! Por favor, informe um cep válido.</div>');
                            } else {
                                //if passes the validation does the rest of code

                                // if user clicked the checkbox, its value is true or false. Need to convert.
                                addressData.save_in_address_book = self.saveInAddressBook ? 1 : 0;

                                // New address must be selected as a shipping address
                                newShippingAddress = createShippingAddress(addressData);
                                selectShippingAddress(newShippingAddress);
                                checkoutData.setSelectedShippingAddress(newShippingAddress.getKey());
                                checkoutData.setNewCustomerShippingAddress(addressData);
                                self.getPopUp().closeModal();
                                self.isNewAddressAdded(true);
                            }
                        }
                    }

                    xhttp.send();
                }
            },

            /**
             * Shipping Method View
             */
            rates: shippingService.getShippingRates(),
            isLoading: shippingService.isLoading,
            isSelected: ko.computed(function () {
                return quote.shippingMethod() ?
                    quote.shippingMethod()['carrier_code'] + '_' + quote.shippingMethod()['method_code'] :
                    null;
            }),

            /**
             * @param {Object} shippingMethod
             * @return {Boolean}
             */
            selectShippingMethod: function (shippingMethod) {
                selectShippingMethodAction(shippingMethod);
                checkoutData.setSelectedShippingRate(shippingMethod['carrier_code'] + '_' + shippingMethod['method_code']);

                return true;
            },

            /**
             * Set shipping information handler
             */
            setShippingInformation: function () {
                var self = this;

                if (this.validateShippingInformation()) {
                    quote.billingAddress(null);
                    checkoutDataResolver.resolveBillingAddress();
                    registry.async('checkoutProvider')(function (checkoutProvider) {
                        var shippingAddressData = checkoutData.getShippingAddressFromData();

                        if (shippingAddressData) {
                            checkoutProvider.set(
                                'shippingAddress',
                                $.extend(true, {}, checkoutProvider.get('shippingAddress'), shippingAddressData)
                            );
                        }
                    });

                    if (quote.shippingAddress().countryId !== 'BR') {
                        setShippingInformationAction().done(
                            function () {
                                stepNavigator.next();
                            }
                        );
                    } else {
                        if (quote.shippingAddress().postcode !== null) {
                            var shippingPostcode = quote.shippingAddress().postcode.replace(/[^0-9]/gi, '');
                            fetch("https://api.svcs.biz/v1/location/" + shippingPostcode).then((response) => {
                                if (response.status == 200) {
                                    setShippingInformationAction().done(
                                        function () {
                                            stepNavigator.next();
                                        }
                                    )
                                } else {
                                    self.errorValidationMessage($t('Invalid Postcode, verify your postcode.'));
                                }
                            }).catch((error) => {
                                self.errorValidationMessage($t('Invalid Postcode, verify your postcode.'));
                            });
                        } else {
                            this.errorValidationMessage($t('Invalid Postcode, verify your postcode.'));
                        }
                    }
                }
            },

            /**
             * @return {Boolean}
             */
            validateShippingInformation: function () {
                var shippingAddress,
                    addressData,
                    loginFormSelector = 'form[data-role=email-with-possible-login]',
                    emailValidationResult = customer.isLoggedIn(),
                    field,
                    option = _.isObject(this.countryOptions) && this.countryOptions[quote.shippingAddress().countryId],
                    messageContainer = registry.get('checkout.errors').messageContainer;

                if (quote.shippingMethod().carrier_code == 'mpmultishipping') {
                    if (
                        typeof quote.shippingMethod().sellerShipping === 'undefined'
                        || quote.shippingMethod().sellerShipping.length != this.totalSelectedSeller()
                    ) {
                        this.errorValidationMessage($t('Please specify a shipping method for each seller.'));
                        return false;
                    }
                }

                if (!quote.shippingMethod()) {
                    this.errorValidationMessage(
                        $t('The shipping method is missing. Select the shipping method and try again.')
                    );

                    return false;
                }

                if (!customer.isLoggedIn()) {
                    $(loginFormSelector).validation();
                    emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
                }

                if (this.isFormInline) {
                    this.source.set('params.invalid', false);
                    this.triggerShippingDataValidateEvent();

                    if (!quote.shippingMethod()['method_code']) {
                        this.errorValidationMessage(
                            $t('The shipping method is missing. Select the shipping method and try again.')
                        );
                    }

                    if (emailValidationResult &&
                        this.source.get('params.invalid') ||
                        !quote.shippingMethod()['method_code'] ||
                        !quote.shippingMethod()['carrier_code']
                    ) {
                        this.focusInvalid();

                        return false;
                    }

                    shippingAddress = quote.shippingAddress();
                    addressData = addressConverter.formAddressDataToQuoteAddress(
                        this.source.get('shippingAddress')
                    );

                    //Copy form data to quote shipping address object
                    for (field in addressData) {
                        if (addressData.hasOwnProperty(field) &&  //eslint-disable-line max-depth
                            shippingAddress.hasOwnProperty(field) &&
                            typeof addressData[field] != 'function' &&
                            _.isEqual(shippingAddress[field], addressData[field])
                        ) {
                            shippingAddress[field] = addressData[field];
                        } else if (typeof addressData[field] != 'function' &&
                            !_.isEqual(shippingAddress[field], addressData[field])) {
                            shippingAddress = addressData;
                            break;
                        }
                    }

                    if (customer.isLoggedIn()) {
                        shippingAddress['save_in_address_book'] = 1;
                    }
                    selectShippingAddress(shippingAddress);
                } else if (customer.isLoggedIn() &&
                    option &&
                    option['is_region_required'] &&
                    !quote.shippingAddress().region
                ) {
                    messageContainer.addErrorMessage({
                        message: $t('Please specify a regionId in shipping address.')
                    });

                    return false;
                }

                if (!emailValidationResult) {
                    $(loginFormSelector + ' input[name=username]').focus();

                    return false;
                }

                return true;
            },

            /**
             * Trigger Shipping data Validate Event.
             */
            triggerShippingDataValidateEvent: function () {
                this.source.trigger('shippingAddress.data.validate');

                if (this.source.get('shippingAddress.custom_attributes')) {
                    this.source.trigger('shippingAddress.custom_attributes.data.validate');
                }
            },

            totalSellerUpdate: function (data, model) {
                if (data.carrier_code != 'mpmultishipping') {
                    model.selectedSellerMethod([]);
                    model.totalSelectedSeller(0);
                    model.totalSellerAmount(0);
                    model.totalBaseSellerAmount(0);
                    $.each($(".seller-method-table input[type='radio']"), function () {
                        $(this).prop("checked", false);
                    });

                    $(".ceperror") ? $(".ceperror").remove() : null;
                }
                if (window.location.hash !="#payment") {
                    selectShippingMethodAction(data);
                    checkoutData.setSelectedShippingRate(data.carrier_code);
                }
            },

            getFormattedPrice: function (price) {
                return priceUtils.formatPrice(price, quote.getPriceFormat());
            },

            /**
             * checks if product wise shipping is enabled from admin config
             */
            productWiseEnabled: function () {

                if (window.productWiseShippingEnabled) {
                    return true;
                }
                return false;
            },

            /**
             * @param {Object} shippingMethod
             * @return {Boolean}
             */
            selectSellerShippingMethod: function (model, multishipping, sellerdata, price, base_amount, selectedMethod, methodName) {
                var productWiseEnabled = self.scope.productWiseEnabled();
                var oldsellerid = 0;
                $(".selected-methods").remove();
                if (model.selectedSellerMethod().length == 0) {
                    model.selectedSellerMethod.push({sellerid: sellerdata.seller_id, itemid: sellerdata.item_ids, price: price, baseamount:base_amount , code: selectedMethod, method: methodName});
                    model.totalSelectedSeller(1);
                } else {
                    var isAdmin = false;
                    $.each(model.selectedSellerMethod(), function (index, value) {
                        if (productWiseEnabled) {
                            if (sellerdata.item_ids == value.itemid ) {
                                oldsellerid = sellerdata.seller_id;
                            }
                        } else {
                            if (sellerdata.seller_id == value.sellerid) {
                                oldsellerid = sellerdata.seller_id;
                            }
                        }
                        if (sellerdata.seller_id == 0 && value.sellerid == 0) {
                            isAdmin = true;
                        }
                    });

                    if (oldsellerid) {
                        console.log("if");
                        $.each(model.selectedSellerMethod(), function (index, value) {
                            if (productWiseEnabled) {
                                if (_.isEqual(sellerdata.item_ids, value.itemid)) {
                                    model.selectedSellerMethod()[index].price = price;
                                    model.selectedSellerMethod()[index].baseamount = base_amount;
                                }
                            } else {
                                if (_.isEqual(value.sellerid, oldsellerid)) {
                                    model.selectedSellerMethod()[index].price = price;
                                    model.selectedSellerMethod()[index].baseamount = base_amount;
                                    model.selectedSellerMethod()[index].method = methodName;
                                }
                            }
                        });
                    } else {
                        console.log("else");
                        model.selectedSellerMethod.push({sellerid: sellerdata.seller_id, itemid: sellerdata.item_ids, price: price, baseamount:base_amount , code: selectedMethod, method: methodName});
                        model.totalSelectedSeller(model.totalSelectedSeller()+1);
                    }
                }
                if (model.selectedSellerMethod().length > 0) {
                    model.totalSellerAmount(0);
                    model.totalBaseSellerAmount(0);
                    $.each(model.selectedSellerMethod(), function (index, value) {
                        model.totalSellerAmount(model.totalSellerAmount() + parseFloat(value.price))
                        model.totalBaseSellerAmount(model.totalBaseSellerAmount() + parseFloat(value.baseamount));
                    });
                }
                $('#co-shipping-method-form').append("<input class='selected-methods' type='hidden' name = 'selected_shipping' value='"+JSON.stringify(model.selectedSellerMethod())+"'/>");
                return true;
            }
        });
    });
